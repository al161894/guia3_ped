﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIA3_PED
{
    class Program
    {
        static void Main(string[] args)
        {           
           /* bool flag = false;

            do
            {
                try
                {
                    Console.WriteLine("\tBienvenido, ejemplos de listas: ");
                    Console.WriteLine("\ta. Insertar al frente");
                    Console.WriteLine("\tb. Insertar al final");
                    Console.WriteLine("\tc. Insertar en una posición");
                    Console.WriteLine("\td. Eliminar al frente");
                    Console.WriteLine("\te. Eliminar al final");
                    Console.WriteLine("\tf. Mostrar lista");
                    Console.WriteLine("\tg. Salir");
                    Console.Write("\nIngrese una opción: ");
                    string op = Console.ReadLine();

                    switch (op.ToLower())
                    {
                        case "a":
                            Console.WriteLine("Escogiste a");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "b":
                            Console.WriteLine("Escogiste b");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "c":
                            Console.WriteLine("Escogiste c");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "d":
                            Console.WriteLine("Escogiste d");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "e":
                            Console.WriteLine("Escogiste e");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "f":
                            Console.WriteLine("Escogiste f");
                            flag = true;
                            Console.ReadKey();
                            Console.Clear();
                            break;
                        case "g":
                            Console.WriteLine("Hasta la proxima!");                            
                            Console.ReadKey();
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("\nLa opción ingresada no existe, presiona cualquier tecla para continuar...");
                            flag = true;
                            Console.ReadKey();
                            System.Console.Clear();
                            break;
                    }
                }
                catch
                {
                    Console.Write("Ha ocurrido un problema con la entrada de datos, vuelve a intentar...");
                    flag = true;
                }
            } while (flag == true);
            flag = false;

            Console.ReadKey();
            */
            //creamos una instancia de la clase lista para que podamos utilizar los metodos
            lista listanueva = new lista();

            //le agregamos cuatro nodos a la lista
            listanueva.InsertarI(40);
            listanueva.InsertarI(30);
            listanueva.InsertarI(20);
            listanueva.InsertarI(10);
            //mostramos que hay contenido dentro de la lista
            listanueva.mostrar();
            //insertando en posicion
            listanueva.InsertarP(220, 2);
            listanueva.mostrar();
            Console.ReadKey();            
        }
    }
}
