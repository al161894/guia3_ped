﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIA3_PED
{
    class lista
    {
        public nodo inicio;//cabeza(puntero) de la lista

        //constructor por defecto
        public lista()
        {
            inicio = null;//al inicio la lista no tiene nodos, por tanto el puntero inicio debe apuntar a null
        }

        //metodo para insertar al final de la lista, como lo hace un ArrayList
        public void InsertarF(int item)
        {
            nodo auxiliar = new nodo();//nodo temporal que todavia no pertenece a la lista
            auxiliar.dato = item;//almacena en el atributo dato el valor recibido en el parametro
            auxiliar.siguiente = null;//hace que el puntero señale a null;

            if(inicio == null)//verifica si la lista esta vacia
            {
                inicio = auxiliar;//hacemos que nodo aux sea parte de la lista, lo hacemos la cabeza de la lista
            }
            else
            {
                nodo puntero;//utilizamos este nodo para recorrer la lista, asi no se mueve el puntero cabeza
                puntero = inicio;//situamos a un puntero señalando al mismo nodo que inicio
                while(puntero.siguiente != null)
                {
                    puntero = puntero.siguiente;//se desplaza por todos los nodos de la lista
                }
                //hacemos que el ultimo nodo que apuntaba a null ahora apunte al nuevo nodo que se ingresa,
                //el nuevo nodo ahora es el que apunta a null indicando el fin de la lista
                puntero.siguiente = auxiliar;//hacemos que ultimo nodo ahora señale al auxiliar
            }
        }

        //metodo para insertar al inicio (insertar en la cabeza)
        public void InsertarI(int item)
        {
            nodo auxiliar = new nodo();//nodo temporal que despues se agrega a la lista

            auxiliar.dato = item;//almacena el valor en el atrib dato
            auxiliar.siguiente = null;//hacemo que puntero siguiente apunte a null

            if(inicio == null)
            {
                inicio = auxiliar;
            }
            else
            {
                nodo puntero;//para recorrer la lista
                puntero = inicio;//dos punteros señalando al mismo nodo
                inicio = auxiliar;//asignamos como cabeza al nodo auxiliar y en la var puntero queda el valor antiguo de la cabeza
                auxiliar.siguiente = puntero;//el puntero de auxiliar que ahora es cabeza se enlace con
                //el nodo que era antes la cabeza y que tiene como apuntador puntero                
            }
        }

        //metodo para eliminar nodo que esta a la cabeza de la lista
        public void EliminarI()
        {
            if(inicio == null)//cuando lista este vacia
            {
                Console.WriteLine("Lista vacia, no se puede eliminar elemento");
            }
            else
            {
                inicio = inicio.siguiente;//a quien estaba señalando inicio será nuevo inicio
            }
        }

        //metodo para eliminar nodo al final de la lista
        public void EliminarF()
        {
            if(inicio == null)//cuando lista este vacia
            {
                Console.WriteLine("Lista vacia, no se puede eliminar elemento");
            }
            else
            {
                nodo punteroant, punteropost;//requiero dos punteros para mover porque no declare la cola
                punteroant = inicio;//inicializo ambos en la cabeza de la lista
                punteropost = inicio;

                while(punteropost.siguiente != null)//mientras puntero posterior no señale a null
                {
                    punteroant = punteropost;//el puntero anterior sera a quien señale el posterior
                    punteropost = punteropost.siguiente;//puntero posterior sera a quien  señalaba antes el posterior
                }
                punteroant.siguiente = null;//con esto sacamos el que estaba al final de la lista, el ultimo nodo
                //era el que estaba señalando el punteropost pero ahora el ultimo será en donde se quedo punteroant
            }
        }

        //metodo para insertar en una posicion especifica de la lista
        public void InsertarP(int item, int pos)
        {
            nodo auxiliar = new nodo();//definicion de nuevo nodo con sus atributos
            auxiliar.dato = item;
            auxiliar.siguiente = null;

            if(inicio == null)//si lista esta vacia
            {
                Console.WriteLine("La lista está vacia, por lo tanto se va a insertar en la 1ra posición");
                inicio = auxiliar;//insertar nodo
            }
            else
            {
                nodo puntero;
                puntero = inicio;//para recorrer la lista
                if(pos == 1)//si la posicion que pidieron es la 1, se inserta en la cabeza
                {
                    inicio = auxiliar;//se guarda en la cabeza el nuevo nodo
                    auxiliar.siguiente = puntero;//el nuevo nodo apunta al nodo que anteriormente era la cabeza
                }
                else//si la posicion solicitada no es la 1
                {
                    //cuando la posicion es dos, no se entra al for
                    for(int i=1; i < pos - 1; i++)
                    {
                        puntero = puntero.siguiente;
                        Console.Write(puntero);
                        if (puntero.siguiente == null)//si llegamos al final de la lista
                            break;
                    }

                    nodo punteronext;//puntero de ayuda
                    punteronext = puntero.siguiente;//a quien señalaba el puntero ahi se ubicara el puntero punteronext
                    puntero.siguiente = auxiliar;//ahora el apuntador puntero señalara al nodo auxiliar(el nuevo)
                    auxiliar.siguiente = punteronext;//el nodo recien ingresado señalara a punteronext
                    //con estas cuatro lineas anteriores es como se corta momentaneamente la lista y luego se une de nuevo
                }
            }
        }

        //metodo que muestra el contenido de la lista
        public void mostrar()
        {
            if(inicio == null)//si la lista esta vacia
            {
                Console.WriteLine("La lista esta vacia");
            }
            else
            {
                nodo puntero;
                puntero = inicio;//inicializamos a puntero en el mismo nodo que la cabeza
                Console.Write("{0} -> \t", puntero.dato);//imprimir valor de nodo. Write y no WriteLine
                while(puntero.siguiente != null)
                {
                    puntero = puntero.siguiente;//avanzamos un nodo en la lista
                    Console.Write("{0} -> \t", puntero.dato);//usar write para que no salte de linea
                }
            }
            Console.WriteLine();
        }
    }//fin de la clase lista
}
